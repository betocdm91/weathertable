//
//  ItemTableViewCell.swift
//  WeatherApp
//
//  Created by Jorge Carrillo on 16/7/18.
//  Copyright © 2018 Jorge Carrillo. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBOutlet weak var lblClima: UILabel!
    @IBOutlet weak var lblCiudad: UILabel!
}
