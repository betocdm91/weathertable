//
//  Item.swift
//  WeatherApp
//
//  Created by Jorge Carrillo on 16/7/18.
//  Copyright © 2018 Jorge Carrillo. All rights reserved.
//

import Foundation
import RealmSwift

class Item:Object {
    @objc dynamic var id:String?
    @objc dynamic var ciudad:String?
    @objc dynamic var clima:String?
    
    override static func primaryKey() -> String?{
        return "id"
    }
    
}
