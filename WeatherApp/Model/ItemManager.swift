//
//  ItemManager.swift
//  WeatherApp
//
//  Created by Jorge Carrillo on 16/7/18.
//  Copyright © 2018 Jorge Carrillo. All rights reserved.
//
import Foundation
import RealmSwift

class ItemManager {
    var items:[Item] = []
    var realm:Realm
    
    init() {
        realm = try! Realm()
        print(realm.configuration.fileURL ?? "")
    }
    
    func addItem(ciudad:String, clima:String){
        let item = Item()
        item.id = "\(UUID())"
        item.ciudad = ciudad
        item.clima = clima
        
        try! realm.write {
            realm.add(item)
        }
    }
    
    func getItems(){
        items = Array(realm.objects(Item.self))
    }
}
