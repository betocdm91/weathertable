//
//  HugoViewController.swift
//  WeatherApp
//
//  Created by Jorge Carrillo on 25/6/18.
//  Copyright © 2018 Darwin Guzmán. All rights reserved.
//

import UIKit

class HugoViewController: UIViewController {

    @IBOutlet weak var cityTextField: UITextField!
    
    @IBOutlet weak var resultLabel: UITextView!
    
    
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBAction func searchButtonPressed(_ sender: Any) {
        let networking = NetworkWrad()
        networking.getWeather(of: cityTextField.text ?? "Cuenca"){(weather) in
            DispatchQueue.main.async {
                self.resultLabel.text = weather.description
                //
                networking.getIconWeather(iconCode: weather.icon, completionHandler: { (imageR) in
                    DispatchQueue.main.async {
                        self.weatherImageView.image = imageR
                    }
                })
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
