//
//  JorgeWeather.swift
//  WeatherApp
//
//  Created by JorgeCarrillo on 25/06/18.
//  Copyright © 2018 JorgeCarrillo. All rights reserved.
//

import Foundation

/*
 "main_weather": "rain"
    struct WradWeatherInfo: Decodable {
        let weather:[SebasWeather]
        let mainWeather:String
 
        enum CodingKeys: String, CodingKeys{
        case weather
        case mainWeather = "main_weather"
        }
    }
 */

struct WradWeatherInfo: Decodable {
    let weather:[WradWeather]

}

struct WradWeather: Decodable{
    let id:Int
    let description:String
    let icon:String
}
