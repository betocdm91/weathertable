//
//  AmbatoViewController.swift
//  WeatherApp
//
//  Created by JorgeCarrillo on 29/5/18.
//  Copyright © 2018 JorgeCarrillo. All rights reserved.
//

import UIKit

class AmbatoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{

    @IBOutlet weak var resultLabel: UITextView!
    
    @IBOutlet weak var cityTextField: UITextField!
    
    @IBOutlet weak var weatherImageView: UIImageView!
    
    @IBOutlet weak var itemsTableView: UITableView!
    @IBOutlet weak var itemsTableViewController: UITableView!
    let itemManager = ItemManager()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func searchButtonPressed(_ sender: Any) {
        let networking = NetworkWrad()
        networking.getWeather(of: cityTextField.text ?? "Ambato"){(weather) in
            DispatchQueue.main.async {
                self.resultLabel.text = weather.description
                //
                self.itemManager.addItem(ciudad: self.cityTextField.text!, clima: weather.description)
                self.itemManager.getItems()
                self.itemsTableView.reloadData()
                networking.getIconWeather(iconCode: weather.icon, completionHandler: { (imageR) in
                    DispatchQueue.main.async {
                        self.weatherImageView.image = imageR
                    }
                })
            }
        }
    }
    //ViewController solo para llamar cosas no hacer nada desde aqui

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    override func viewWillAppear(_ animated: Bool) {
        itemManager.getItems()
        itemsTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemManager.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "item_cell") as! ItemTableViewCell
        cell.lblCiudad.text = itemManager.items[indexPath.row].ciudad
        cell.lblClima.text = itemManager.items[indexPath.row].clima
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Historial"
    }
}
